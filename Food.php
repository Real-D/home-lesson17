<?php

class Food extends Item
{
    public $price;

    public function __construct($title, $type, $price)
    {
        parent::__construct($title, $type, $price);
        $this->price = $price;
    }

    public function getType()
     {
        return 'Food';
     }

     public function getPrice()
     {
        return $this->price - ($this->price * 0.1);
     }
}