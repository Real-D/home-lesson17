<?php

abstract class Item implements Writable
{
    private $title;
    private $type;
    private $price;

    public function __construct($title, $type, $price)
    {
        $this->title = $title;
        $this->type = $this->getType();
        $this->price = $price;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getType()
    {
        return 'base_item';
    }

    public function getSummaryLine()
    {
        $res = '<h1>Title: </h1>' . $this->title . ' <p>Price: ' . $this->price . '</p><p>Type: ' . $this->type . '</p>';
        return $res;
    }

    abstract public function getPrice();
}
