<?php
require_once 'Writable.php';
require_once 'Item.php';
require_once 'GoodsItem.php';
require_once 'Food.php';
require_once 'Bonus.php';
require_once 'ItemsWriter.php';

$food = new Food('bread', 'f', '100');
$good = new GoodsItem('Sony', 'g', '20000', '100');
$bonus = new Bonus('first pay', 'b', 'first free');

echo $good->getPrice();
echo $food->getType();
echo $good->getSummaryLine();
echo '<br><br>';
echo '<br><br>';

$res = new ItemsWriter();

$items = [
    $res->addItem($good),
    $res->addItem($food),
    $res->addItem($bonus)
];

echo $res->write($items);
