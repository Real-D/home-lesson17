<?php

class GoodsItem extends Item
{
    public $price;
    public $discount;

    public function __construct($title, $type, $price, $discount)
    {
        parent::__construct($title, $type, $price);
        $this->price = $price;
        $this->discount = $discount;
    }

    public function getType()
    {
        return 'goods';
    }

    public function getPrice()
    {
        return $this->price - $this->discount;
    }
}