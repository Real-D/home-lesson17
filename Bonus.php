<?php

class Bonus implements Writable
{
    private $title;
    private $type;
    private $description;

    public function __construct($title, $type, $description)
    {
        $this->title = $title;
        $this->type = $type;
        $this->description = $description;
    }

    public function getSummaryLine()
    {
        $res = '<h1>Title: </h1>' . $this->title . '<p>Type: ' . $this->type . '</p><p>Description: ' . $this->description . '</p>';
        return $res;
    }
}